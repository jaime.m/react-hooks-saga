import React, {useState} from 'react';
import {Container,Typography, Card, Grid,TextField,Button} from '@material-ui/core'
import {MovieIcon} from '../../icons/';
import styles from './style'
export default ()=>{
    const [searchText,setSearchText]=useState('');
    const classes=styles();


    const handleSearchTextChange=event=>{
        setSearchText(event.target.value);
    };
    const handleCleanClick=event=>{
        setSearchText('');
    };
    const handleSeachClick=event=>{
        console.log('hola search');
    }
    console.log(searchText);
    return (
        <Container className={classes.container}>
            <Card className={classes.cardContainer}>
                <Grid className={classes.titleGridContainer}>
                <Grid>
                    <Typography className={classes.title}>Bienvenido!</Typography>
                    </Grid>
                    <Grid>
                        <MovieIcon className ={classes.iconazo}/>
                    </Grid>
                </Grid>
                <TextField
                    value={searchText}
                    className={classes.titleFieldSearch}
                    placeholder ="Buscar..."
                    onChange={handleSearchTextChange}
                />
                <Grid className={classes.buttonsContainer}>
                    <Button variant="contained" onClick ={handleCleanClick}>Clean</Button>
                    <Button variant="contained" className={classes.searchButton} onClick ={handleSeachClick} color ="primary" size="large">Search</Button>
                </Grid>


            </Card>
            </Container>
    )
}